package service;

import domain.Homework;
import domain.Student;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.junit.jupiter.params.ParameterizedTest;
import org.junit.jupiter.params.provider.ValueSource;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.junit.jupiter.MockitoExtension;
import repository.HomeworkXMLRepository;
import repository.StudentXMLRepository;
import validation.ValidationException;

import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertThrows;
import static org.mockito.ArgumentMatchers.any;
import static org.mockito.Mockito.verify;
import static org.mockito.Mockito.when;

@ExtendWith(MockitoExtension.class)
class ServiceMockTest {

    @Mock
    private StudentXMLRepository studentXMLRepository;

    @Mock
    HomeworkXMLRepository homeworkXMLRepository;

    @InjectMocks
    private Service service;


    @Test
    public void saveStudentSuccessfully() {
        Student mockedStudent = new Student("111", "Test Name", 150);
        when(studentXMLRepository.save(any())).thenReturn(mockedStudent);
        assertEquals(0, service.saveStudent("111", "Test Name", 150));
    }

    @Test
    public void saveStudentWithInvalidGroupId() {
        when(studentXMLRepository.save(any())).thenThrow(new ValidationException("exception test"));
        assertThrows(ValidationException.class, () -> service.saveStudent("12", "Test Name", 80));
    }

    @Test
    public void deleteStudentWithExistingId() {
        Student mockedStudent = new Student("111", "Test Name", 150);
        when(studentXMLRepository.delete(mockedStudent.getID())).thenReturn(mockedStudent);
        assertEquals(1, service.deleteStudent(mockedStudent.getID()));
    }

    @ParameterizedTest(name = "{index} => message= Homework ''{0}'' updated.")
    @ValueSource(strings = {"1", "2", "3"})
    public void updateHomeworkWithExistingId(String id) {
        Homework mockedHomework = new Homework(id, "Test_" + id, 9, 7);
        when(homeworkXMLRepository.update(any())).thenReturn(mockedHomework);
        assertEquals(1, service.updateHomework(id, "Test_" + id, 9, 7));
        verify(homeworkXMLRepository).update(mockedHomework);
    }

    @ParameterizedTest(name = "{index} => message= Homework ''{0}'' updated.")
    @ValueSource(strings = {"12", "22", "32"})
    public void updateHomeworkWithNonExistingId(String id) {
        Homework mockedHomework = new Homework(id, "Test_" + id, 9, 7);
        when(homeworkXMLRepository.update(any())).thenThrow(new ValidationException("test exception."));
        assertThrows(ValidationException.class, () -> service.updateHomework(id, "Test_" + id, 9, 7));
        verify(homeworkXMLRepository).update(mockedHomework);
    }
}