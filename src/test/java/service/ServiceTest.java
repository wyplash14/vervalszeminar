package service;

import domain.Grade;
import domain.Homework;
import domain.Student;
import org.junit.jupiter.api.BeforeAll;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.params.ParameterizedTest;
import org.junit.jupiter.params.provider.ValueSource;
import repository.GradeXMLRepository;
import repository.HomeworkXMLRepository;
import repository.StudentXMLRepository;
import validation.GradeValidator;
import validation.HomeworkValidator;
import validation.StudentValidator;
import validation.Validator;

import static org.junit.jupiter.api.Assertions.*;

class ServiceTest {

    static Service service;
    static Validator<Student> studentValidator;
    static Validator<Homework> homeworkValidator;
    static Validator<Grade> gradeValidator;

    @BeforeAll
    public static void setUp() throws Exception {
        studentValidator = new StudentValidator();
        homeworkValidator = new HomeworkValidator();
        gradeValidator = new GradeValidator();
    }

    @BeforeEach
    public void setUpService() throws Exception {
        StudentXMLRepository fileRepository1 = new StudentXMLRepository(studentValidator, "students.xml");
        HomeworkXMLRepository fileRepository2 = new HomeworkXMLRepository(homeworkValidator, "homework.xml");
        GradeXMLRepository fileRepository3 = new GradeXMLRepository(gradeValidator, "grades.xml");

        service = new Service(fileRepository1, fileRepository2, fileRepository3);
    }


    @Test
    public void saveStudentSuccessfully() {
        long length = service.findAllStudents().spliterator().getExactSizeIfKnown();
        assertEquals(1, service.saveStudent("117", "Lazar David", 150), "If new student was added successfully it should return 1");
        assertEquals(service.findAllStudents().spliterator().getExactSizeIfKnown(), length + 1);
    }

    @Test
    public void saveHomeworkWithNullId() {
        long length = service.findAllHomework().spliterator().getExactSizeIfKnown();
        assertNotEquals(0, service.saveHomework(null, "test description", 20, 15), "If save was not successful it should not return 0");
        assertNotEquals(service.findAllHomework().spliterator().getExactSizeIfKnown(), length + 1);
    }

    @Test
    public void deleteStudentWithExistingId() {
        service.saveStudent("300","Test",150);
        long length = service.findAllStudents().spliterator().getExactSizeIfKnown();
        assertTrue(1 == service.deleteStudent("300"));
        assertTrue((length - 1) == service.findAllStudents().spliterator().getExactSizeIfKnown());
    }

    @ParameterizedTest(name = "{index} => message= Homework ''{0}'' updated.")
    @ValueSource(strings = {"1", "2", "3"})
    public void updateHomeworkWithExistingId(String id) {
        assertEquals(1, service.updateHomework(id, "Test_" + id, 9, 7));
    }

    @ParameterizedTest(name = "{index} => message= Homework ''{0}'' updated.")
    @ValueSource(strings = {"12", "22", "32"})
    public void updateHomeworkWithNonExistingId(String id) {
        assertNotEquals(1, service.updateHomework(id, "Test_" + id, 9, 7));
    }
}